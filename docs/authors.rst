Authors
========

Contributors of code, tests and documentation to the project who have agreed
to have their work enjoined into the project and project license (BSD).

* Jeff Hinrichs <dundeemt@gmail.com>, http://inre.dundeemt.com/