Change Log
==========


v0.2.0
------
Planned...

* Deprecate and remove mergeDicts


v0.1.8
------
Planned...

* DeprecationWarning for mergeDicts


v0.1.7
------
In Progress..

* refactored code from module file to a proper package layout
* reimplement mergeDicts as merge_dicts

  * PendingDeprecationWarning for mergeDicts function
  * added tests for PendingDep warning and refactored existing tests to use merge_dicts
  * expanded test suite

* pep8 clean up of code
* add Change Log to documentation
* created a linting tool for config file, yjlint
* moved to yaml.safe_load, added option to allow old style, safe=False

v0.1.6
-------
Current Release (released Apr 2014)

* migrated from code.google.com to bitbucket
* set up continuous integration

  * drone.io
  * tox

* implemented testing

  * py.test
  * tested explicitly on python 27, 32, 33, 34
  * anecdotally believed to run without issue on python 26

* wrote documentation, hosted on readthedocs.org
* started using twine to upload to pypi

v0.1.3 - v0.1.5
---------------
Did battle with long_description and pypi

v0.1.2
------
Previous Version (released Oct 2009)

* code hosted on code.google.com
* package hosted on code.google.com
* supported current python 26

v0.1.0 - v.0.1.1
----------------
(released Mar 2009)

* alpha-1 and alpha-2 releases