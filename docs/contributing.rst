Contributing
============
You can contribute to the project in a number of ways.  Code is always good,
bugs are interesting but telling a friend or a peer about YamJam is awsome.

Code
-----

1.  Fork the repository on `Bitbucket <https://bitbucket.org/dundeemt/yamjam>`_ .

2.  Make a virtualenv, clone the repos, install the deps from `pip install -r requirements-dev.txt`

3.  Write any new tests needed and ensure existing tests continue to pass without modification.

  a.  Setup CI testing on drone.io for your Fork.  See `current script <https://drone.io/bitbucket.org/dundeemt/yamjam/admin>`_ .

4.  Ensure that your name is added to the end of the :doc:`authors` file using the format Name <email@domain.com> (url), where the (url) portion is optional.

5.  Submit a Pull Request to the project on Bitbucket.



Bug Reports
-----------
If you encounter a bug or some surprising behavior, please file an issue on our `tracker <https://bitbucket.org/dundeemt/yamjam/issues?status=new&status=open>`_


Get the Word Out
-----------------
* Tell a friend
* email a list
* blog about about it
* give a talk to your local users group

Let's face it, a config system is not that exciting (unless you get it wrong - Windows Registry).  But just like templating, everyone needs it.  You roll your own, you try ton's of if/then and environment detection but it always ends up lacking.   YamJam makes it sane and installable from pypi.


Now instead of spending attention cycles on your own boring implementation, you can spend your time on  interesting code and let us take of the mundane.

.. image:: https://bytebucket.org/dundeemt/yamjam/raw/15e9582ebedf024cc6ff64d8aa29dfc3aea261e6/yamjam-logo.png
  :width: 48px
