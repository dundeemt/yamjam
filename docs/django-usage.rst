Django Usage
============

--------------------------------
Django Scenario - Sensitive Data
--------------------------------
settings.py contains a number of configuration settings that should be kept secret/private and not be included in commits to source code control.  So you either don't check in your settings.py file (which creates its own set of problems) or your roll your own system.  YamJam takes care of the tedious for you.

Example
-------
*settings.py*

.. code:: python

  from YamJam import yamjam
  ...
  DJANGO_SECRET_KEY = yamjam()['myproject']['django_secret_key']
  ...

*~/.yamjam/config.yaml*

.. code:: yaml

  myproject:
      django_secret_key: my-secret-key-value


-----------------------------------------------------------------------------
Django Scenario - Differing Settings for Development/ Staging/ and Production
-----------------------------------------------------------------------------
Your dev system uses a local data store for on the go development and to keep any migrations or bad data out of your staging and/or production environments.  This means you need a different settings.py for each environment.  Ok, there is a problem.  Wouldn't you prefer one settings.py file with the specialized settings handled without writing lots of if/else statements?  Enter YamJam


Example
-------

*settings.py*

.. code:: python

    from YamJam import yamjam
    ...
    dbcfg = yamjam()['myproject']['database']

    DATABASES = {
        'default': {
            'ENGINE':dbcfg['engine'],
            'NAME': dbcfg['name'],
            'USER': dbcfg['user'],
            'PASSWORD': dbcfg['password'],
            'HOST': dbcfg['host'],
            'PORT': dbcfg['port'],
        }
    }

*~/.yamjam/config.yaml   - on dev machine*

.. code:: yaml

  myproject:
      django_secret_key: my-secret-key-value
      database:
        engine: django.db.backends.postgresql_psycopg2
        name: mydatabase
        user: mydatabaseuser
        password: mypassword
        host: 127.0.0.1
        port: 5432


*~/.yamjam/config.yaml   - on production machine*

.. code:: yaml

  myproject:
      django_secret_key: my-secret-key-value
      database:
        engine: django.db.backends.postgresql_psycopg2
        name: mydatabase
        user: mydatabaseuser
        password: mypassword
        host: 10.0.0.24
        port: 5432

Notice how you can use one settings.py, with sensitive information masked. Combined with different ~/.yamjam/config.yaml on each machine makes a drop dead simple, deploy by version control system.


Huzzah!
******************
We keep the cold stuff cold and the hot stuff hot -- Or should I
say YamJam keeps the versioned stuff versioned and the private stuff private.


