DRY Scenario
=============

Multiple apps that access common resources
------------------------------------------
Do you ever write apps that integrate with other resources?  Do you write
additional apps that also integrate or report on those resources?  If you find
yourself not following the Don't Repeat Yourself (DRY) conventions for
resource information then YamJam is your friend.


* App1 access the corporate accounting db and the mail server and your no-sql data to automate reporting to vendors or customers.
* App2 access the corporate accounting db and your no-sql data and your fax system to generate POs.


Change Happens
**************
Your accounting db just moved to new hardware and now has a new address and user credentials.  What do you do?  Maybe each app has a settings/config file and the info is not hard coded but you still have to update the resource information in multiple places.   Oh yeah, you need development set ups for those apps too.

Handle It.
******************
If you use YamJam you'll have a single point to update resource information, dev and production systems settings.  Win, Win, Win!  YamJam is not just for Django it is for any app that needs to be able to keep private information private and create a DRY environment for your shared resource configurations.


Advanced DRY usage
-------------------
The following is not specific to YamJam as it is in the YAML specs.

YAML allows for `Alias indicators <http://www.yaml.org/refcard.html>`_ which allow for the following example:

.. code:: yaml

    bill-to: &id001
        given  : Monty
        family : Python
    ship-to: *id001

which would return the following:

.. code:: python

    {'bill-to': {'given': 'Monty', 'family': 'Python'}, 'ship-to': {'given': 'Monty', 'family': 'Python'}}

This is helpful when you wish to refactor a key for a setting but you have other apps still relying on the old key.  Until you can refactor those apps to use the new key, you can use an Alias to keep your information `DRY <http://en.wikipedia.org/wiki/Don't_repeat_yourself>`_
