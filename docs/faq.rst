Frequently asked questions
==========================


Why yaml for persistence?
-------------------------
All config dialects have their warts, some have more than others. However,
YAML, in our opinion, is the most pythonic of the bunch.  White space is
relevant in yaml just like Python.  Also, the visual look of yaml for dictionaries and lists is very congruous to Python.

.. code:: yaml

    key: value
    adict:
        key: value
        key2: value2
    alist: [1, 2, 3, 4]


How do I write to my config?
----------------------------
The short answer, is with your editor.  YamJam is designed for change infrequently, read frequently scenarios.  Your Django apps don't write to settings.py, we don't write to disk either.


Security
---------
YamJam data is secured in the same method your .ssh directory is secured, `chmod 700`.  It doesn't support encryption intrinsically.  It can return an encrypted value your have put there but that is up to your app. So follow the install instructions and `chmod 700` your ~/.yamjam directory.


What is so special about YamJam, I could roll my own
-----------------------------------------------------
Not a thing is technically special about YamJam.  Yes, it's got a decent mergeDict routine.  The problem with rolling your own is the same type of problem as rolling your own templating system.  Why not just `pip install yamjam` instead of maintaining the code yourself.  That is the thing about YamJam, it's easy to install.  It takes the mundane task and rolls it up so you can write more exciting code.  Just for the mere fact you can simply install it and then start factoring out sensitive and installation dependent data that has leaked into your repos.


What's up with the name, YamJam?
--------------------------------
One day I was sitting there, dealing with an issue of config differences between dev and production and I said to myself, wtf? Why can't I just take my config settings and jam it in a yaml file?  Hence, YamJam.

