.. YamJam documentation master file, created by
   sphinx-quickstart on Thu Apr 24 22:42:49 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to YamJam's documentation!
==================================
.. image:: https://bytebucket.org/dundeemt/yamjam/raw/15e9582ebedf024cc6ff64d8aa29dfc3aea261e6/yamjam-logo.png
  :width: 48px

*keeping private data out of source control and applying DRY principles for resource information since 2009*

--------
Overview
--------
is a multi-project, shared, yaml based configuration system. It is also a mechanism to keep secret/private data from leaking out to source control systems (i.e. git, bitbucket, svn, et al) by factoring out sensitive data from your commits.

Tested on Python 2.7, 3.2, 3.3, 3.4

.. image:: https://drone.io/bitbucket.org/dundeemt/yamjam/status.png
   :target: https://drone.io/bitbucket.org/dundeemt/yamjam/latest
   :alt: Build Status


------------
Installation
------------

.. code:: console

  pip install yamjam
  mkdir ~/.yamjam
  touch ~/.yamjam/config.yaml
  chmod -R go-rwx ~/.yamjam

Now open ~/.yamjam/config.yaml with your editor of choice and start factoring out your sensitive information.  :doc:`organizing-your-config`

-----------
What Next?
-----------

* **License**: BSD

* `Documentation <http://yamjam.rtfd.org/>`_

* `Project <https://bitbucket.org/dundeemt/yamjam>`_

* `Contributing <http://yamjam.readthedocs.org/en/latest/contributing.html>`_

* `Change Log <http://yamjam.readthedocs.org/en/latest/changes.html>`_

We work so well with Django, you'd think we should spell our name YamDjam

Contents:

.. toctree::
   :maxdepth: 2

   organizing-your-config
   django-usage
   dry-resources
   faq
   contributing
   releasing
   changes
   authors

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

