Release Procedures
==================
Steps necessary for an uneventful release of YamJam.

Full Release
------------

1. Merge in outstanding Pull requests
  a. ensure your local repos is up to date ``hg sum --remote``
2. Ensure test suite pass / drone.io

.. image:: https://drone.io/bitbucket.org/dundeemt/yamjam/status.png
   :target: https://drone.io/bitbucket.org/dundeemt/yamjam/latest
   :alt: Build Status

3. run `release.sh --dry-run` and ensure all looks well
4. run ``release.sh --tag --commit`` and make sure to push any outstanding commits to master
  a. ``hg ci -m"Bumpversion 0.x.y -> 0.x'y'"``
5. create a new empty environment and ``pip install /localpath/yamjam-x.y.z.gz``
6. start a python shell, import and test base functionality

.. code:: python

    from YamJam import yamjam
    yamjam()

7. upload package to pypi, ``twine upload sdist/*``
8. verify new dist is available on pypi
9. Be Happy


Build Dist Only
---------------

.. code:: console

    python setup.py sdist


Install and test dist locally
-----------------------------

1. Build Dist Only
2. Create clean environment
3. install local dist file in clean environment

.. code:: console

    pip install /localpath/to/dist/yamjam-x.y.z.gz

4. start a python shell, import and test base functionality

.. code:: python

    from YamJam import yamjam
    yamjam()

5. remove testing environment