'''test active deprecations, if any - uses py.test'''

# the following 3 lines let py.test find the module
import sys, os
MYPATH = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, MYPATH + '/../')

import warnings

# what we are testing
from YamJam import mergeDicts


def test_mergedicts_pending_dep():
    '''test the PendingDeprecationWarning in mergeDicts, 0.1.7 - deprecate 0.1.8
    and remove in 0.2.0'''

    with warnings.catch_warnings(record=True) as wrng:
        # Cause all warnings to always be triggered.
        warnings.simplefilter("always")
        # Trigger a warning.
        _ = mergeDicts({}, {})
        # Verify some things
        assert len(wrng) == 1
        assert issubclass(wrng[-1].category, PendingDeprecationWarning)
        assert "deprecated" in str(wrng[-1].message)
