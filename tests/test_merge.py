'''test the YamJam.merge_dicts function - uses py.test'''

# the following 3 lines let py.test find the module
import sys, os
MYPATH = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, MYPATH + '/../')

# what we are testing
from YamJam import merge_dicts


def test_docstring():
    '''orignal test from the docstring'''
    base = {'a':1, 'b':{'b1':1, 'b3':3, 'b5':'5'}, 'd':[4, 5]}
    oride = {'c':1, 'b':{'b2':2, 'b4':4, 'b5':5, 'b6':6}, 'd':[1, 2, 3]}
    assert merge_dicts(base, oride) == {'a': 1,
                                       'c': 1,
                                       'b': {
                                             'b4': 4,
                                             'b5': 5,
                                             'b6': 6,
                                             'b1': 1,
                                             'b2': 2,
                                             'b3': 3},
                                        'd': [1, 2, 3]}

def test_simple_replacement():
    '''replace key-value with new value from override(oride)'''
    base = {'a':1}
    oride = {'a': 2}
    assert merge_dicts(base, oride) == {'a': 2}

def test_simple_append():
    '''append new key-value from oride'''
    base = {'a':1}
    oride = {'b': 2}
    assert merge_dicts(base, oride) == {'a': 1, 'b':2}

def test_advanced_replacement():
    '''override an existing key-value'''
    base = {'a': {'b':1}}
    oride = {'a': None}
    assert merge_dicts(base, oride) == {'a': None}

def test_advanced_append():
    '''override an existing nested key-value'''
    base = {'a': {'b': 1}}
    oride = {'a': {'c': 2}}
    assert merge_dicts(base, oride) == {'a': {'b': 1, 'c': 2}}

def test_docs_merge_with_empty_dict():
    '''an example from the docs'''
    base = {'key1': {'foo': 2}}
    oride = {}
    assert merge_dicts(base, oride) == {'key1': {'foo': 2}}

def test_docs_merge_dicts():
    '''an example from the docs'''
    base = {'key1': {'foo': 2}}
    oride = {'key1': {'bar': 3}}
    assert merge_dicts(base, oride) == {'key1': {'foo': 2, 'bar': 3}}

