'''test the YamJam.yamjam function - uses py.test'''

from dhp.test import tempfile_containing
import pytest

# the following 3 lines let py.test find the module
import sys, os
MYPATH = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, MYPATH + '/../')

# what we are testing
from YamJam import yamjam, YAMLError


def test_tempfile_containing():
    '''test tempfile_containing contextmgr'''
    contents = "Mary had a little lamb"
    with tempfile_containing(contents, suffix='.yaml') as fname:

        with open(fname) as fhndl:
            assert fhndl.read() == contents

    assert os.path.exists(fname) == False


def test_simples():
    '''test a simple yaml primitives, and show they have a type'''
    yamls = 'duck: quack\nfrog: 1\nsnail: 1.0'
    with tempfile_containing(yamls, suffix='.yaml') as fname:
        cfg = yamjam(fname)
        assert cfg['duck'] == 'quack'
        assert cfg['frog'] == 1
        assert cfg['snail'] == 1.0
        assert type(cfg['frog']) == type(int())
        assert type(cfg['snail']) == type(float())


def test_broken_yaml_space():
    '''test a broken yaml, a space after the : is required'''
    yamls = 'duck:quack\nfrog: 1\nsnail: 1.0'
    with tempfile_containing(yamls, suffix='.yaml') as fname:
        with pytest.raises(YAMLError):
            yamjam(fname)


def test_broken_yaml_tab():
    '''test a broken yaml, tabs are bad mojo in yaml'''
    yamls = 'duck:\tquack\nfrog: 1\nsnail: 1.0'
    with tempfile_containing(yamls, suffix='.yaml') as fname:
        with pytest.raises(YAMLError):
            yamjam(fname)


def test_yaml_tab_in_value():
    '''test a simple yaml primitive'''
    yamls = 'duck: "quack\tquack"\nfrog: 1\nsnail: 1.0'
    with tempfile_containing(yamls, suffix='.yaml') as fname:
        cfg = yamjam(fname)
        assert cfg['duck'] == 'quack\tquack'


def test_simple_merge_override():
    '''test a simple override'''
    ybase = 'duck: quack\ndog: woof'
    yoride = 'duck: honk'

    with tempfile_containing(ybase) as fbase:
        with tempfile_containing(yoride) as foride:
            cfg = yamjam('%s;%s' % (fbase, foride))
            assert cfg['duck'] == 'honk'
            assert cfg['dog'] == 'woof'
            assert cfg == {'duck':'honk', 'dog':'woof'}

def test_simple_merge_append():
    '''test a simple override'''
    ybase = 'duck: quack\ndog: woof'
    yoride = 'duck: honk\ncat: meow'

    with tempfile_containing(ybase) as fbase:
        with tempfile_containing(yoride) as foride:
            cfg = yamjam('%s;%s' % (fbase, foride))
            assert cfg == {'duck':'honk', 'dog':'woof', 'cat':'meow'}

def test_single_config():
    '''issue 19, empty config raises TypeError'''
    ybase = ''

    with tempfile_containing(ybase) as fbase:
        cfg = yamjam('%s' % fbase)
        assert cfg == {}
