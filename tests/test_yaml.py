'''test / display yaml assumptions/specs made in the documenation'''

from YamJam import yaml


def test_alias():
    '''confirm yaml aliases work as expected'''
    ybuf = """
    bill-to: &id001
        given  : Monty
        family : Python
    ship-to: *id001
    """
    expected = {'bill-to': {
                    'given': 'Monty',
                    'family': 'Python'},
                'ship-to': {
                    'given': 'Monty',
                    'family': 'Python'}}
    assert yaml.load(ybuf) == expected

def test_lists():
    '''confirm yaml list dialect'''
    ybuf = """
    myproject:
        key1: value
        key2: [42, 21, 7]
    """
    expected = [42, 21, 7]
    assert yaml.load(ybuf)['myproject']['key2'] == expected

