'''test the yjlint utility'''
from dhp.test import tempfile_containing
import pytest

# the following 3 lines let py.test find the module
import sys, os
MYPATH = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, MYPATH + '/../')

# what we are testing
from YamJam.yjlint import lint_yamjam


def test_passing_cfg(capsys):
    '''check a valid yaml config'''
    ybase = 'duck: quack'

    with tempfile_containing(ybase) as fname:
        lint_yamjam(fname)

    out, err = capsys.readouterr()
    assert 'Confirmed: Valid YAML' in out
    assert err == ''

def test_bad_cfg_tabs(capsys):
    '''check a bad yaml config with tabs'''
    ybase = 'duck: quack\nfrog:\tcroak'

    with tempfile_containing(ybase) as fname:
        with pytest.raises(SystemExit):
            lint_yamjam(fname)

    out, err = capsys.readouterr()
    assert 'Error position: (2:6)' in out
    assert err == ''

def test_bad_cfg_not_a_dict(capsys):
    '''check a bad yaml, not a dictionary'''
    ybase = 'duck:quack\n'

    with tempfile_containing(ybase) as fname:
        with pytest.raises(SystemExit):
            lint_yamjam(fname)

    out, err = capsys.readouterr()
    assert 'ERROR: yaml does not evaluate to a dictionary' in out
    assert err == ''

